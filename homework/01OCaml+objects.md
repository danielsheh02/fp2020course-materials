### OCaml + объекты

###### Ast

- Классы, объекты. Тип классов по желанию (на доп. баллы, если договоримся)
- Методы публичные и привантные. Рекурсивные вызовы планируется делать через методы (т.е. `let rec` определения не обязательно)
- Мутабельные поля классов.
- `let` определения новых значений.
- Стандартные типы
  ** числа и арифметические операции (объявления своих и переопределение операций не надо)
  ** Строки и операции со строками не обязательно.
  ** Синтаксис списков и сопоставление со образцом только для списков чего-либо.
  ** Объявление пользовательских алг. типов по желанию (на доп. баллы, если договоримся)
- Типовые аннотации, вывод типов, проверка типов как в настоящем OCaml.

###### Parser (2 балла)

Синтаксис подсматривать у настоящего OCaml.

###### Интерпретатор (до 7 баллов)

Если вывод/проверка типов нужны.

Должны быть тесты как минимум на

- Рекурсию
- Работу с приватными полями.
- Перекрытие имен в блоках.
- Какие-нибудь структуры данных с объектами (например, реализация стека) с помощью объектов

###### Program transformations

Стандартные: переименование полей или переименование методов (1 балл), поиск недостижимого кода (2 балла), может что-то ещё
