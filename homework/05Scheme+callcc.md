### язык Scheme c поддержкой Call-with-current-continuation

###### Parser (1 балл)

Ast здесь почти вырожденное, потому что парсер очень простой. Поэтому только 1 балл.

###### Интерпретатор (до 6 баллов)

- Списки, работа с ними.
- Числа и операции с ними.
- Сопоставление с образцом по желанию. Если без него, то должны быть какие-то стандартные функции для работы с списками и т.п.
- Объявления функий.
- Quote/Unquote
- Присваивание не обязательно
- Разумеется, лямбды, иначе это не Scheme
- Реализация вызова call/cc. (Здесь будет сложно. Что это такое и зачем нужно найдите в инете сами)

Тесты

- Про рекурсию (фибоначчи и т.п.)
- Про работу со списками.
- Про Quote/unquote
- Про call/cc
- и т.п.



Умные ссылки
- [ПФП: Паттерны использования «call with current continuation»](http://fprog.ru/lib/ferguson-dwight-call-cc-patterns/)



Файлик с тестами в помощь студенту 

```scheme
(define fac (lambda (n)
  (cond
  ((< n 1) 1)
  (else (* n (fac (- n 1))))
)))

(assert (= 120 (fac 5)))
(printf "~a\n" (fac 5))
#|
(define fac2 (lambda (n)
  (let helper ( ((acc n) (1 n)) )
         (cond
            ((< n 1) acc)
            (else (helper ((* acc n) (- n 1)))))
       )
)) |#


;; from http://www.cs.uni.edu/~jacobson/scheme/session10.html
(define factorial-aps
  (lambda (n answer)
    (if (zero? n)
        answer
        (factorial-aps (- n 1) (* n answer)))))

(define fac2
    (lambda (n)
      (factorial-aps n 1)))


(assert (= 120 (fac2 5)))
(printf "~a\n" (fac2 5))


;; https://rosettacode.org/wiki/Category:Scheme
;; In this example  Y should be called Zed
(define Y                 ; (Y f) = (g g) where
  (lambda (f)             ;         (g g) = (f  (lambda a (apply (g g) a)))
    ((lambda (g) (g g))   ; (Y f) ==        (f  (lambda a (apply (Y f) a)))
     (lambda (g)
       (f  (lambda a (apply (g g) a)))))))

;; head-recursive factorial
(define fac                ; fac = (Y f) = (f      (lambda a (apply (Y f) a)))
  (Y (lambda (r)           ;     = (lambda (x) ... (r     (- x 1)) ... )
       (lambda (x)         ;        where   r    = (lambda a (apply (Y f) a))
         (if (< x 2)       ;               (r ... ) == ((Y f) ... )
             1             ;     == (lambda (x) ... (fac  (- x 1)) ... )
             (* x (r (- x 1))))))))

;; tail-recursive factorial
(define fac2
  (lambda (x)
    ((Y (lambda (r)        ;       (Y f) == (f     (lambda a (apply (Y f) a)))
          (lambda (x acc)  ;          r         == (lambda a (apply (Y f) a))
            (if (< x 2)    ;         (r ... )   == ((Y f) ... )
                acc
                (r (- x 1) (* x acc))))))
     x 1)))

; double-recursive Fibonacci
(define fib
  (Y (lambda (f)
       (lambda (x)
         (if (< x 2)
             x
             (+ (f (- x 1)) (f (- x 2))))))))

; tail-recursive Fibonacci
(define fib2
  (lambda (x)
    ((Y (lambda (f)
          (lambda (x a b)
            (if (< x 1)
                a
                (f (- x 1) b (+ a b))))))
     x 0 1)))

(assert (= 720 (fac 6)))
(assert (= 233 (fib2 13)))
;(printf "~a\n" (fac 6))
;(printf "~a\n" (fib2 13))




;; Mutual recursion test
(define error-message-positive "Error. x must be a nonnegative number")

(define odd-positive?
  (lambda (x)
    (cond
      ((not (integer? x)) error-message-number)
      ((= x 0) #f)
      ((< x 0) error-message-positive) ;for negatives
      (else (even? (- x 1)))))) ;if number n is odd then n - 1 is even

(define even-positive?
  (lambda (x)
    (cond
      ((not (integer? x)) error-message-number)
      ((= x 0) #t)
      ((< x 0) error-message-positive) ;for negatives
      (else (odd? (- x 1))))))



(assert (even-positive? 4))
(assert (not (even-positive? 5)))
(assert (not (odd-positive? 6)))
(assert (odd-positive? 7))


;;
;; call-cc
;; https://ds26gte.github.io/tyscheme/index-Z-H-15.html
(assert (= 4
      (+ 1 (call/cc
       (lambda (k)
         (+ 2 (k 3)))))))

(define list-product
  (lambda (s)
    (let recur ((s s))
      (if (null? s) 1
          (* (car s) (recur (cdr s)))))))

(assert (= 120 (list-product '(1 2 3 4 5))))

(define list-product
  (lambda (s)
    (call/cc
      (lambda (exit)
        (let recur ((s s))
          (if (null? s) 1
              (if (= (car s) 0) (exit 0)
                  (* (car s) (recur (cdr s))))))))))

(assert (= 120 (list-product '(1 2 3 4 5))))
(assert (= 0 (list-product '(1 2 3 4 0 5))))


;; tree matching

(define same-fringe?
  (lambda (tree1 tree2)
    (let loop ((ftree1 (flatten tree1))
               (ftree2 (flatten tree2)))
      (cond ((and (null? ftree1) (null? ftree2)) #t)
            ((or (null? ftree1) (null? ftree2)) #f)
            ((eqv? (car ftree1) (car ftree2))
             (loop (cdr ftree1) (cdr ftree2)))
            (else #f)))))

(define flatten
  (lambda (tree)
    (cond ((null? tree) '())
          ((pair? (car tree))
           (append (flatten (car tree))
                   (flatten (cdr tree))))
          (else
           (cons (car tree)
                 (flatten (cdr tree)))))))

(assert (same-fringe? '(1 (2 3)) '((1 2) 3)) )
(assert (not (same-fringe? '(1 2 3) '(1 (3 2))) ))



(define tree->generator
  (lambda (tree)
    (let ((caller '*))
      (letrec
          ((generate-leaves
            (lambda ()
              (let loop ((tree tree))
                (cond ((null? tree) 'skip)
                      ((pair? tree)
                       (loop (car tree))
                       (loop (cdr tree)))
                      (else
                       (call/cc
                        (lambda (rest-of-tree)
                          (set! generate-leaves
                            (lambda ()
                              (rest-of-tree 'resume)))
                          (caller tree))))))
              (caller '()))))
        (lambda ()
          (call/cc
           (lambda (k)
             (set! caller k)
             (generate-leaves))))))))

(define same-fringe2?
  (lambda (tree1 tree2)
    (let ((gen1 (tree->generator tree1))
          (gen2 (tree->generator tree2)))
      (let loop ()
        (let ((leaf1 (gen1))
              (leaf2 (gen2)))
          (if (eqv? leaf1 leaf2)
              (if (null? leaf1) #t (loop))
              #f))))))

(assert (same-fringe2? '(1 (2 3)) '((1 2) 3)) )
(assert (not (same-fringe2? '(1 2 3) '(1 (3 2))) ))
```
