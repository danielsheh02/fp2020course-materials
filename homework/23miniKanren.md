### miniKanren

Разновидность логического программирования. Знакомиться по книжке Reasoned Schemer.

###### Ast

- Создание констант из функциональных символов и других констант.
- Задание реляций (отношений), в том числе рекурсивных
- Создание свежих (экзистенциальных) переменных
- Дизъюнкция с интерливингом (conde)
- Конъюнкция

###### Parser (2 балла)

###### Интерпретатор (до 6 баллов)

Тесты брать из книжки, потом выдам ещё дополнительно


```scheme
;; use with https://github.com/michaelballantyne/faster-miniKanren
(include "./mk-vicare.scm")
(include "./mk.scm")
(include "./test-check.scm")


(define (nullo x)
  (== '() x))

(printf "~a\n"
  (run 1 (q) (nullo q)))

(define (conso a d p)
  (== `(,a . ,d) p))

(define (caro p a)
  (fresh (d)
    (== (cons a d) p)))

(define (cdro p d)
  (fresh (a)
    (== (cons a d) p)))

(define (appendo l t out)
  (conde
    ((nullo l) (== t out))
    ((fresh (a d res)
       (conso a d l)
       (conso a res out)
       (appendo d t res)))))

(printf "~a\n"
  (run* (x y) (appendo x y '(1 2 3 4 5))))

(define (reverso xs out)
  (conde
    ((nullo xs) (== xs out))
    ((fresh (h tl res)
       (conso h tl xs)
       (reverso tl res)
       (appendo res `(,h) out)))))

(printf "~a\n"
  (run* (xs) (reverso '(1 2 3 4 5) xs)))

(printf "~a\n"
  (run 1 (xs) (reverso xs '(1 2 3 4 5) )))

; hangs
;(printf "~a\n"
;  (run* (xs) (reverso xs '(1 2 3 4 5) )))


; TODO: realtional interpeter and quines
; TODO: disequality constraints?


```
