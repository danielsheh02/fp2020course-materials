let label (type u) (f : (u -> _) -> u) : u =
  let exception R of u in
  try f (fun x -> raise (R x)) with R u -> u

let f () : int =
  label (fun return ->
      if true then return 1;
      if true then return 2;
      3 (* result *))

let%test _ = 1 = f ()

(*  *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *)
exception Finally of exn * exn

let protectx ~f x ~finally =
  match f x with
  | res ->
      finally x;
      res
  | exception exn ->
      raise
        ( match finally x with
        | () -> exn
        | exception final_exn -> Finally (exn, final_exn) )

(*  *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *)

(** Call [hd xs] returns head of the list and
   raises execption Failure when list is empty  *)
let hd_exn xs = match xs with [] -> failwith "Bad argument: hd" | x :: _ -> x

let hd xs = match xs with [] -> None | x :: _ -> Some x
