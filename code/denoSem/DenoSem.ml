module type SYMANTICS = sig
  type t
  val const : int -> t
  val add : t -> t -> t
end

module Printer : (SYMANTICS with type t = string) = struct
  type t = string
  let const = string_of_int
  let add = Printf.sprintf "(%s+%s)"
end

module Eval : (SYMANTICS with type t = float) = struct
  type t = float
  let const = float_of_int
  let add = (+.)
end

module Example (S: SYMANTICS) = struct
  open S
  let demo1 = add (const 1) (const 10)
end

module P = Example(Printer)
module E = Example(Eval)

let () =
  Printf.printf "Meaning of %s is %f\n%!" P.demo1 E.demo1
