open ULC

let __ =
  Format.printf "%a\n%!" Lam.pp
    ( Result.get_ok
    @@ Angstrom.parse_string
         (parse_lam.single parse_lam)
         "(z t)" ~consume:Angstrom.Consume.Prefix )

let () = Format.printf "Failed tests: %d\n%!" (ULC.TestQCheck.run ())
