type expr = Int of int | Add of expr * expr [@@deriving show]

module type SYMANTICS = sig
  type t

  val int : int -> t
  val add : t -> t -> t
  val to_int : t -> int
  val show : t -> string
end

module Example (S : SYMANTICS) = struct
  let make assoc n =
    (* Format.printf "make called\n%!"; *)
    let open S in
    let rec helper acc n =
      (* Format.printf "helper: i=%d acc = '%s'\n%!" i (S.show acc); *)
      if n <= 0 then acc
      else
        let acc = if assoc then add acc (int 1) else add (int 1) acc in
        helper acc (n - 1) in
    helper (int 1) n
end

module Shallow : SYMANTICS = struct
  type t = int

  let int n = n
  let add = ( + )
  let to_int x = x

  (* let () = assert (2 = to_int (add (int 1) (int 1))) *)
  let show = string_of_int
end

module Deep : SYMANTICS = struct
  type t = expr

  let int n = Int n
  let add l r = Add (l, r)

  let to_int =
    let rec helper = function Int n -> n | Add (l, r) -> helper l + helper r in
    helper

  let show = show_expr
end

open Benchmark

let () =
  let wrap tr () =
    let (module L : SYMANTICS) = tr in
    let module E = Example (L) in
    let expr = E.make true 100 in
    assert (101 = L.to_int expr) in
  tabulate
  @@ throughputN ~repeat:1 1
       [ ("Deep", wrap (module Deep : SYMANTICS), ())
       ; ("Shallow", wrap (module Shallow : SYMANTICS), ()) ]
