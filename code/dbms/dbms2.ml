open Common

(* Упрощенное представление термов по сравнению с предыдущим случаем *)
module RAISE (M : MONADFAIL) : sig
  type 'a t =
    | Return : 'a -> 'a t
    | Raise : exc -> 'a t
    | PromoteBind : 'b M.t * ('b -> 'a t) -> 'a t

  include TRANSFORMER with type 'a m = 'a M.t and type 'a t := 'a t
  include MONADFAIL with type 'a t := 'a t

  val raise : exc -> 'a t
end = struct
  (*
  Законы R1
  Законы O3
  *)
  type 'a m = 'a M.t
  type exc = string

  type 'a t =
    | Return : 'a -> 'a t
    | Raise : exc -> 'a t
    | PromoteBind : 'b m * ('b -> 'a t) -> 'a t

  let raise exc = Raise exc
  let return x = Return x
  let promote m = PromoteBind (m, return)
  let fail e = Raise e

  let rec ( >>= ) x f =
    match x with
    | Return x -> f x
    | Raise exc -> Raise exc
    | PromoteBind (m, k) -> PromoteBind (m, fun a -> k a >>= f)

  let rec observe = function
    | Return a -> M.return a
    | Raise exc -> M.fail exc
    | PromoteBind (m, f) -> M.( >>= ) m (fun x -> observe (f x))
end
