(* cata === "вовнутрь", "вниз" *)

let cata_ = List.fold_right

let rec cata f e xs =
  match xs with [] -> e | x :: xs -> f x (cata f e xs)

(* Сортировка вставками *)
let isort xs =
  let rec insert x lst =
    match lst with
    | [] -> [ x ]
    | h :: xs when x < h -> x :: h :: xs
    | h :: xs -> h :: insert x xs
  in
  cata_ insert xs []

let%test _ = isort [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ]

let%test _ = isort [ 2; 1; 4; 3 ] = [ 1; 2; 3; 4 ]

module BuggyPara = struct
  let rec paraL f e xs =
    match xs with
    | [] -> e
    | x :: xs -> f x (xs, paraL f e xs)

  let isort3 lt xs =
    let insert3 x lst =
      paraL
        (fun h (tl, acc) ->
          if lt x h then x :: h :: tl else h :: acc)
        [ x ] lst
    in
    cata_ insert3 xs []

  let%test _ = isort3 ( < ) [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ]

  let%test _ = isort3 ( < ) [ 4; 3; 2; 1 ] = [ 1; 2; 3; 4 ]
end

module GoodPara = struct
  let rec paraL f e xs =
    match xs with
    | [] -> e
    | x :: xs -> f x (xs, lazy (paraL f e xs))

  let isort3 lt xs =
    let insert3 x lst =
      paraL
        (fun h (tl, acc) ->
          if lt x h then x :: h :: tl
          else h :: Lazy.force acc)
        [ x ] lst
    in
    List.fold_right insert3 xs []

  let%test _ = isort3 ( < ) [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ]

  let%test _ = isort3 ( < ) [ 4; 3; 2; 1 ] = [ 1; 2; 3; 4 ]
end

module CheckCounter = struct
  let () =
    let counter = ref 0 in
    let lt a b =
      incr counter;
      a < b
    in
    assert (
      BuggyPara.isort3 lt [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ] );
    assert (6 = !counter)

  let () =
    let counter = ref 0 in
    let lt a b =
      incr counter;
      a < b
    in
    assert (
      GoodPara.isort3 lt [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ] );
    assert (3 = !counter)
end

(* ***** Unfold -- дуальная функция к fold  *********** *)
let rec unfold :
    ('a -> ('b * 'a Lazy.t) option) -> 'a -> 'b list =
 fun f u ->
  match f u with
  | None -> []
  | Some (x, v) -> x :: unfold f (Lazy.force v)

let rec unfold2 :
    ('a -> bool) ->
    ('a -> 'b) ->
    ('a -> 'a) ->
    'a ->
    'b list =
 fun fin get next b ->
  if fin b then []
  else get b :: unfold2 fin get next (next b)

(* Анаморфизм -- "преобразование наружу" (греч.) *)
let ana1 = unfold

let ana = unfold2

module SelectionSort = struct
  let minimumL = function
    | x :: xs -> List.fold_left min x xs
    | _ -> failwith "bad argument"

  let rec deleteL y = function
    | [] -> []
    | x :: xs when y = x -> xs
    | x :: xs -> x :: deleteL y xs

  let delmin = function
    | [] -> None
    | xs ->
        let y = minimumL xs in
        Some (y, lazy (deleteL y xs))

  let sort xs = ana1 delmin xs

  let%test _ = sort [ 4; 3; 2; 1 ] = [ 1; 2; 3; 4 ]
end

(* ******* Сортировка пузырьком через unfold ******** *)
module BubbleSort = struct
  let bubble xs =
    (* С каждым шагом [x] всплывает *)
    let step x = function
      | None -> Some (x, lazy [])
      | Some (y, ys) when x < y ->
          Some (x, lazy (y :: Lazy.force ys))
      | Some (y, ys) -> Some (y, lazy (x :: Lazy.force ys))
    in
    cata step None xs

  let sort xs = ana1 bubble xs

  let%test _ = sort [ 4; 3; 2; 1 ] = [ 1; 2; 3; 4 ]
end

(**  Упражнение
  * Если описать стрим как тип
  *    type 'a stream = Nil | Cons of 'a * 'a string Lazy.t
  *  то функция "дайте n минимальных элементов в стриме" пишется как
  *    let take_n_smallest xs = take n (sort xs)
  *)

(* TODO: apomorphisms *)

(* Hylomophimsm: fold after unfold *)
let fact n =
  assert (n >= 0);
  cata ( * ) 1 (ana (( = ) 0) Fun.id pred n)

let%test _ = ana (( = ) 0) Fun.id pred 5 = [ 5; 4; 3; 2; 1 ]

let%test _ = fact 5 = 120

let hyloL f e p g h eta = cata f e (ana p g h eta)

let fact = hyloL ( * ) 1 (( = ) 0) Fun.id pred

let%test _ = fact 5 = 120

(* N.B. deforestated implementation *)
let rec hyloL f e p g h eta =
  if p eta then e else hyloL f (f e (g eta)) p g h (h eta)

(* Proof by Term Rewriting *)

(*
  let hyloL f e p g h eta = cata f e (ana p g h eta)
  let hyloL f e p g h eta =
    if p eta
    then cata f e []
    else
      cata f e (g eta :: ana p g h (h eta))

  let hyloL f e p g h eta =
    if p eta
    then e
    else
      cata f (f e (g eta)) (ana p g h (h eta))

  let rec hyloL f e p g h eta =
    if p eta
    then e
    else
      hyloL f (f e (g eta)) p g h (h eta)
  *)

let%test _ =
  let fact = hyloL ( * ) 1 (( = ) 0) Fun.id pred in
  fact 5 = 120

(*
  Упражение: Напишите функцию char list -> bool list, которая конвертирует
  десятичное представление числа в бинарное, с помощью unfold после fold

*)

let rec map f = function
  | [] -> []
  | x :: xs -> f x :: map f xs

let _ = map string_of_int (map (( + ) 1) [ 1; 2; 3; 4; 5 ])

(*
   map f (map g xs) === map (fun x -> f (g x)) xs


  map f (map g xs)

  = function
  | [] -> map f []
  | x :: xs -> map f (g x :: map g xs)


    = function
  | [] -> []
  | x :: xs -> (f (g x)) :: (map f (map g xs))


  h x === f (g x)

  map h = function
  | [] -> []
  | x :: xs -> (h x) :: map h xs

*)
