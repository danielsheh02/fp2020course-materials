(* ***************** Lists ****************************** *)
type ('self, 'a) listlike = Nil | Cons of 'a * 'self

module L = struct
  type nonrec 'a t = ('c, 'a) listlike as 'c

  let cons x xs = Cons (x, xs)

  let nil = Nil

  let rec rev_append sx ys =
    match sx with
    | Nil -> ys
    | Cons (x, sx) -> rev_append sx (cons x ys)

  let rec map f = function
    | Nil -> Nil
    | Cons (x, xs) -> cons (f x) xs

  let rec merge xs ys =
    let rec helper acc = function
      | Nil, ys | ys, Nil -> rev_append acc ys
      | Cons (x, xs), (Cons (y, _) as r) when x < y ->
          helper (cons x acc) (xs, r)
      | l, Cons (y, ys) -> helper (cons y acc) (l, ys)
    in
    helper nil (xs, ys)

  let rec of_list = function
    | [] -> Nil
    | x :: xs -> Cons (x, of_list xs)

  let rec to_list = function
    | Nil -> []
    | Cons (x, xs) -> x :: to_list xs

  (* fold_left  *)
  let rec cata f acc xs =
    match xs with
    | Nil -> acc
    | Cons (x, tl) -> cata f (f acc x) tl

  let length xs = cata (fun acc _ -> acc + 1) 0 xs

  let%test _ = 3 = length (of_list [ 1; 2; 3 ])

  let sum xs = cata ( + ) 0 xs

  let%test _ = 6 = sum (of_list [ 1; 2; 3 ])

  (* unfoldr *)
  let rec ana fin f pred init =
    if fin init then nil
    else cons (f init) (ana fin f pred (pred init))

  let rev xs =
    let rec helper acc = function
      | Nil -> acc
      | Cons (x, xs) -> helper (cons x acc) xs
    in
    helper Nil xs

  let split_at n =
    let rec helper acc n xs =
      if n <= 0 then (rev acc, xs)
      else
        match xs with
        | Nil -> (rev acc, xs)
        | Cons (x, xs) -> helper (cons x acc) (n - 1) xs
    in
    helper Nil n

  let%test _ =
    (of_list [ 1; 5 ], of_list [ 6 ])
    = split_at 2 (of_list [ 1; 5; 6 ])
end

(* fold after unfold *)
let rec hylo f e p g h eta =
  if p eta then e else hylo f (f e (g eta)) p g h (h eta)

let%test _ =
  let n = 10 in
  let xs = List.init n (fun x -> x + 1) in
  let sum xs = L.cata ( + ) 0 (L.of_list xs) in
  n * (n + 1) / 2 = sum xs

let%test _ =
  let fact = hylo ( * ) 1 (( = ) 0) Fun.id pred in
  fact 5 = 120

type 'self natlike = Zero | Succ of 'self

module N = struct
  type t = t natlike

  let zero = Zero

  let succ n = Succ n

  (* For peano number left fold is the same as right fold *)
  let rec cata f acc = function
    | Zero -> acc
    | Succ p -> cata f (f acc) p

  (* unfold *)
  let rec ana fin pred init =
    if fin init then zero
    else succ (ana fin pred (pred init))

  let of_int n = ana (( = ) 0) (fun x -> x - 1) n

  let%test _ = succ (succ zero) = of_int 2

  let to_int : t -> int = cata (fun n -> n + 1) 0

  let%test _ = to_int zero = 0

  let%test _ = to_int (succ zero) = 1

  let%test _ = to_int (succ (succ zero)) = 2

  (* Paramorphism: Храним дополнительно текущее посчитанное значение *)
  let rec para f n =
    match n with
    | Zero -> f Zero
    | Succ p -> f (Succ (p, para f p))

  let natfac =
    let alg = function
      | Zero -> 1
      | Succ (n, f) -> to_int (succ n) * f
    in
    para alg

  let%test _ = natfac (succ (succ (succ zero))) = 6
end

type ('self, 'a) treelike =
  | Leaf
  | Empty of 'a
  | Node of 'self * 'self

module T = struct
  type 'a t = ('a t, 'a) treelike

  (* Smart constructors :) *)
  let leaf = Leaf

  let empty n = Empty n

  let node a b = Node (a, b)

  (* left and then right traversal *)
  let rec cata f acc xs =
    match xs with
    | Leaf -> acc
    | Empty n -> f acc n
    | Node (l, r) -> cata f (cata f acc l) r
end

let rec myhylo :
    (('a -> 'b) -> 'c -> 'd) ->
    ('d -> 'b) ->
    ('a -> 'c) ->
    'a ->
    'b =
 fun fmap alg coalg t ->
  alg (fmap (myhylo fmap alg coalg) (coalg t))

let mergeSort =
  (* T A ->  A *)
  let alg : (int L.t, int) treelike -> int L.t = function
    | Leaf -> L.nil
    | Empty c -> L.(cons c nil)
    | Node (l, r) -> L.merge l r
  in
  (* A -> T A *)
  let coalg = function
    | Nil -> T.leaf
    | Cons (x, Nil) -> T.empty x
    | xs ->
        let l, r = L.split_at (L.length xs / 2) xs in
        T.node l r
  in
  let fmap :
      ((('b, int) listlike as 'b) -> 'b) ->
      ((('a, int) listlike as 'a), int) treelike ->
      ((('c, int) listlike as 'c), int) treelike =
   fun f -> function
    | Leaf -> Leaf
    | Empty n -> Empty n
    | Node (l, r) -> Node (f l, f r)
  in

  (* let treelike_fmap f g = function
       | Leaf -> Leaf
       | Empty x -> Empty (g x)
       | Node (l, r) -> Node (f l, f r)
     in
     let fmap f = treelike_fmap f Fun.id in *)
  myhylo fmap alg coalg

let%test _ =
  L.of_list [ 1; 2; 3 ] = mergeSort (L.of_list [ 3; 2; 1 ])

let%test _ =
  L.of_list [ 1; 2; 3; 4 ]
  = mergeSort (L.of_list [ 3; 2; 1; 4 ])
