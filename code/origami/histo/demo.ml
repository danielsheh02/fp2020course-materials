(* histomorphisms *)

module FibonacciHisto = struct
  type nat = Z | S of nat

  (* https://stackoverflow.com/a/24892711/1065436  *)
  let histo : ('a list -> 'a) -> nat -> 'a =
   fun f ->
    let rec go : nat -> 'a list = function
      | Z -> [ f [] ]
      | S x ->
          let subvalues = go x in
          f subvalues :: subvalues
    in
    fun x -> List.hd (go x)

  (* -- Example: calculate the n-th fibonacci number *)
  let fibN : nat -> int =
    histo (function x :: y :: _ -> x + y | _ -> 1)

  let%test _ = fibN (S Z) = 1

  let%test _ = fibN (S (S Z)) = 2

  let%test _ = fibN (S (S (S Z))) = 3

  let%test _ = fibN (S (S (S (S Z)))) = 5
end

let rec cata fmap phi x = phi (fmap (cata fmap phi) x)

(* hylo === fold after unfold === cata after ana *)

(* dyno === hist after unfold *)

(* Но Хистоморфизм выразим через катаморфизм, поэтому
  динамическое программирование мы будем показывать
  на хиломорфизмах, а не диноморфизмах *)

(* Примеры из статьи: "Recursion schemes for dynamic programming" *)

module FibHylo = struct
  type 'a inter = Zero | One | Two of 'a * 'a
  [@@deriving map]

  let psi : int -> int inter = function
    | 0 -> Zero
    | 1 -> One
    | n -> Two (n - 1, n - 2)

  let phi = function
    | Zero -> 1
    | One -> 1
    | Two (x, y) -> x + y

  let int_unfold fmap f =
    let rec helper n = fmap helper (f n) in
    helper

  let (_ : int -> ('a inter as 'a)) =
    int_unfold map_inter psi

  let hylo phi psi x =
    cata map_inter phi (int_unfold map_inter psi x)

  let fib = hylo phi psi

  let%test _ = fib 0 = 1

  let%test _ = fib 1 = 1

  let%test _ = fib 2 = 2

  let%test _ = fib 3 = 3

  let%test _ = fib 4 = 5
end

(* Binary partitions: количество способов преставить (без повторений)
  число в виде суммы степеней двойки *)
module BP = struct
  type 'a inter = No | One of 'a | Two of 'a * 'a
  [@@deriving map]

  let rec unfold fmap f n = fmap (unfold fmap f) (f n)

  let hylo phi psi x =
    cata map_inter phi (unfold map_inter psi x)

  let psi : int -> int inter = function
    | 0 -> No
    | n when n mod 2 = 1 -> One (n - 1)
    | n -> Two (n - 1, n / 2)

  let phi = function
    | No -> 1
    | One x -> x
    | Two (x, y) -> x + y

  let count = hylo phi psi

  let%test _ = (* 0 = 0 *) count 0 = 1

  let%test _ = (* 1 = 2^0 *) count 1 = 1

  let%test _ = (* 2 = 2^1 = 2^0 + 2^0 *) count 2 = 2

  let%test _ =
    (* 3 = 2^1 + 2^0 = 2^0 + 2^0 + 2^0 *) count 3 = 2
end

module LongestCommonSubsequence = struct
  (* direct style *)
  let lcs xs ys =
    let rec helper = function
      | [], _ -> []
      | _, [] -> []
      | x :: xs, y :: ys when x = y -> x :: helper (xs, ys)
      | x :: xs, y :: ys ->
          let r1 = helper (x :: xs, ys) in
          let r2 = helper (xs, y :: ys) in
          if List.length r1 > List.length r2 then r1 else r2
    in
    helper (xs, ys)

  let%test _ =
    lcs [ 0; 1; 2; 3 ] [ 1; 2; 3; 4 ] = [ 1; 2; 3 ]

  type ('a, 'self) inter =
    | No
    | One of ('a * 'a) * 'self * 'self * 'self
  [@@deriving map]

  (* Our new type is two-paramteric now *)
  let fmap f = map_inter Fun.id f

  let psi :
      'a list * 'a list -> ('a, 'a list * 'a list) inter =
    function
    | [], _ | _, [] -> No
    | x :: xs, y :: ys ->
        One ((x, y), (xs, y :: ys), (x :: xs, ys), (xs, ys))

  let unfold fmap f =
    let rec helper n = fmap helper (f n) in
    helper

  let phi = function
    | No -> []
    | One ((a, b), _, _, x3) when a = b -> a :: x3
    | One ((_, _), x1, x2, _)
      when List.length x1 > List.length x2 ->
        x1
    | One ((_, _), _, x2, _) -> x2

  let hylo phi psi x =
    cata (map_inter Fun.id) phi
      (unfold (map_inter Fun.id) psi x)

  let lcs2 = hylo phi psi

  let%test _ =
    [ 1; 2; 3 ] = lcs2 ([ 0; 1; 2; 3 ], [ 1; 2; 3; 4 ])
end

(*
module T = struct
  type ('a, 'b) t =
    | Leaf of 'a
    | Node of 'b * ('a, 'b) t * ('a, 'b) t

  let sum_direct : (int, unit) t -> int =
    let rec helper acc = function
      | Leaf n -> acc + n
      | Node ((), l, r) -> helper (helper acc l) r
    in
    helper 0

  let list_sum xs = List.fold_left ( + ) 0 xs

  let histo :
      ('a list -> 'a list -> 'a) -> ('a, unit) t -> 'a =
   fun f ->
    let rec go : ('a, unit) t -> 'a list = function
      | Leaf n -> [ f [ n ] [] ]
      | Node ((), l, r) ->
          let l_subvalues = go l in
          let r_subvalues = go r in
          assert ([] <> l_subvalues);
          assert ([] <> r_subvalues);

          f l_subvalues r_subvalues :: l_subvalues
    in

    fun x -> List.hd (go x)

  (* doesn't work as expected *)
  let funny_sum : (int, unit) t -> int =
    histo (fun xs ys -> list_sum xs + list_sum ys)

  let node x l r = Node (x, l, r)

  let node_ l r = Node ((), l, r)

  let leaf x = Leaf x

  let%test _ =
    let s = funny_sum (Leaf 1) in
    Format.printf "%d\n" s;
    s = 1

  type dir = L | R

    let unfold_t =
      let rec helper dir


  let%test _ = funny_sum (node_ (leaf 1) (leaf 5)) = 6

  let%test _ =
    funny_sum
      (node_
         (node_ (leaf 1) (leaf 5))
         (node_ (leaf 3) (leaf 7)))
    = 24
end

(* TODO: upgrade the code from Haskell
https://stackoverflow.com/questions/24884475/examples-of-histomorphisms-in-haskell
*)
(* type 'a nel = Some of 'a | More of 'a * 'a nel *)
(*
type ('a, 'self) nelf = Some of 'a | More of 'a * 'self

let natural : int -> (int, int) nelf = function
  | 0 -> Some 0
  | n -> More (n, n - 1)

module type COMONAD = sig
  type 'a t

  val duplicate : 'a t -> 'a t t
end

include (
  struct
    let zcata fmap duplicate extract z k g =
      let rec c eta =
        k
          (fmap
             (fun eta -> duplicate (fmap g (c eta)))
             (z eta))
      in
      fun eta -> g (extract (c eta))
  end :
    sig
      val zcata :
        (('a -> 'b) -> 'c -> 'd) ->
        ('d -> 'b) ->
        ('c -> 'a) ->
        ('a -> 'c) ->
        ('d -> 'c) ->
        ('a -> 'b) ->
        'a ->
        'b

      (* val zcata :  (Comonad w, Functor f) =>
         (a -> f a) -> (f (w (w c)) -> w b) -> (b -> c) -> a -> c *)
    end )

module Cofree = struct end

(* Control.Comonad.Cofree.unfold :: Functor f => (b -> (a, f b)) -> b -> Cofree f a *)
let cofree_unfold _ _ _ = assert false

(* distGHisto :: (Functor f, Functor h) => (forall b. f (h b) -> h (f b)) -> f (Cofree h a) -> Cofree h (f a) *)
let distGHisto k =
  cofree_unfold (fun as_ ->
      (fmap extract as_, k (fmap Cofree.unwrap as_)))

(* distHisto :: Functor f => f (Cofree f a) -> Cofree f (f a) *)
let distHisto = distGHisto id

(* dyna :: Functor f => (f (Cofree f c) -> c) -> (a -> f a) -> a -> c *)
let dyna phi z = zcata z distHisto phi

(* data Cofree f a  = a :< (f (Cofree f a))  *)

type ('f, 'a) cofree = Cf of 'a * 'f

(* takeC :: Int -> Cofree (NELf a) a -> [a] *)
let rec takeC n x =
  if n = 0 then []
  else
    match x with
    | Cf (a, Some v) -> [ a ]
    | Cf (a, More (v, as_)) -> a :: takeC (n - 1) as_

let list_zip_with f xs ys =
  List.map (fun (a, b) -> f a b) @@ List.combine xs ys

let sum xs = List.fold_left ( + ) 0 xs

(* catalan :: Int -> Int *)
let catalan =
  (* phi :: NELf Int (Cofree (NELf Int) Int) -> Int *)
  let phi : _ -> int = function
    | Some 0 -> 1
    | More (n, table) ->
        let xs = takeC n table in
        sum (list_zip_with ( * ) xs (List.rev xs))
    | _ -> failwith "should not happen"
  in
  dyna phi natural


*)

*)
