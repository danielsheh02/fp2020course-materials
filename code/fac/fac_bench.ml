open Benchmark
open Fac


let () =
  let f = memoize fib_rec in
  Printf.printf "%s %d\n%!" __FILE__ __LINE__;
  ignore (f 45);
  Printf.printf "%s %d\n%!" __FILE__ __LINE__;
  ignore (f 45);
  Printf.printf "%s %d\n%!" __FILE__ __LINE__



let () =
  let res = throughputN ~repeat:1 1
    [ ("recursive", fib_rec, 10)
    ; ("memoized1", memo_rec fib_norec, 10)
    ; ("memoized2", memo_rec2 fib_norec, 10)
    ; ("memoized3", memo_rec3 fib_norec, 10)
    ]
  in
  tabulate res
