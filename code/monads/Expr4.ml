type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string

module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
end

module type MONADERROR = sig
  include MONAD

  val error : string -> 'a t
end

module Eval (M : MONADERROR) = struct
  let eval from_env : expr -> int M.t =
    let open M in
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) -> helper l >>= fun l -> helper r >>= fun r -> return (l + r)
      | Asterisk (l, r) -> helper l >>= fun l -> helper r >>= fun r -> return (l * r)
      | Slash (l, r) ->
        helper r
        >>= fun r ->
        if r = 0 then error "division by zero" else helper l >>= fun l -> return (l / r)
      | Var s -> from_env s
    in
    helper
  ;;
end

module Option : MONADERROR with type 'a t = 'a option = struct
  type 'a t = 'a option

  let ( >>= ) = Option.bind
  let return = Option.some
  let error _ = None
end

let%test _ =
  let module E = Eval (Option) in
  Some 7 = E.eval (fun _ -> None) (Plus (Const 1, Asterisk (Const 2, Const 3)))
;;

module Result = struct
  type 'a t = ('a, string) Result.t

  let ( >>= ) = Result.bind
  let return = Result.ok
  let error = Result.error
end

let%test _ =
  let module E = Eval (Result) in
  Error "not found" = E.eval (fun _ -> Error "not found") (Var "x")
;;

(* ********************** Lists ***************************************** *)
module L = struct
  type 'a t = 'a list

  let ( >>= ) x f = List.concat_map f x
  let return x = [ x ]
  let error _ = []
end

let%test _ =
  let module E = Eval (L) in
  List.sort
    Int.compare
    (E.eval
       (function
         | "x" -> [ -1; 1 ]
         | _ -> [ -10; 10 ])
       (Plus (Var "x", Var "y")))
  = [ -11; -9; 9; 11 ]
;;

(* ********************** Identity ***************************************** *)
module ID = struct
  type 'a t = 'a

  let ( >>= ) x f = f x
  let return x = x
  let error = failwith
end

let%test _ =
  let module E = Eval (ID) in
  E.eval
    (function
      | "x" -> 1
      | _ -> raise Not_found)
    (Plus (Var "x", Const 1))
  = 2
;;

(* ********************** Continuation ************************************* *)
module Cont : sig
  include MONADERROR

  val run_cont : ('a -> 'b) -> 'a t -> 'b
end = struct
  type ('a, 'b) cont = 'a -> 'b
  type 'a t = { cont : 'b. ('a, 'b) cont -> 'b }

  let return (x : 'a) = { cont = (fun k -> k x) }

  let ( >>= ) (x : 'a t) (f : 'a -> 'b t) : 'b t =
    { cont = (fun k -> x.cont (fun v -> (f v).cont k)) }
  ;;

  let error = failwith
  let run_cont f { cont } = cont f
end

let%test _ =
  let module E = Eval (Cont) in
  let env = function
    | "x" -> Cont.return 1
    | _ -> raise Not_found
  in
  2 = Cont.run_cont Fun.id (E.eval env (Plus (Var "x", Const 1)))
;;
