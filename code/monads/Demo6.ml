module type APPLICATIVE = sig
  type 'a t

  val pure : 'a -> 'a t
  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
end

module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
end

module type MONAD_APPLICATIVE_FAIL = sig
  include MONAD
  include APPLICATIVE with type 'a t := 'a t

  val error : string -> 'a t
end

type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string

module Work (M : MONAD_APPLICATIVE_FAIL) = struct
  open M

  let eval from_env : expr -> int M.t =
    let rec helper = function
      | Const n -> return n
      (* | Plus (l, r) -> return ( + ) <*> helper l <*> helper r *)
      | Plus (l, r) -> helper l >>= fun l -> helper r >>= fun r -> return (l + r)
      | Asterisk (l, r) -> return ( * ) <*> helper l <*> helper r
      | Slash (l, r) ->
        return (fun l r -> l, r)
        <*> helper l
        <*> helper r
        >>= (function
        | _, 0 -> error "division by zero"
        | l, r -> return (l / r))
      | Var s -> from_env s
    in
    helper
  ;;
end

(* ********************** Identity ***************************************** *)
module Demo1 = struct
  module M : MONAD_APPLICATIVE_FAIL with type 'a t = 'a = struct
    type 'a t = 'a

    let ( >>= ) x f = f x
    let ( <*> ) f x = f x
    let return x = x
    let pure = return
    let error = failwith
  end

  let _ =
    print_endline "Demo with Identity monad";
    let module E = Work (M) in
    let rez =
      E.eval
        (function
          | "x" ->
            Thread.delay 3.0;
            print_endline "returning x";
            1
          | "y" ->
            Thread.delay 3.0;
            print_endline "returning y";
            2
          | _ -> M.error "no variable")
        (Plus (Var "x", Var "y"))
    in
    Format.printf "%d\n%!" rez
  ;;
end

(* ********************** Threads ***************************************** *)
module Demo2 = struct
  module M : MONAD_APPLICATIVE_FAIL with type 'a t = 'a Lwt.t = struct
    type 'a t = 'a Lwt.t

    let ( >>= ) = Lwt.bind

    let ( <*> ) f x =
      let open Lwt in
      Lwt.both f x >>= fun (f, x) -> Lwt.return (f x)
    ;;

    let return x = Lwt.return x
    let pure = return
    let error = failwith
  end

  let _ =
    print_endline "Demo with Lwt monad";
    let module E = Work (M) in
    let rez =
      let open Lwt in
      E.eval
        (function
          | "x" ->
            Lwt_unix.sleep 3.0
            >>= fun _ ->
            let () = print_endline "returning x" in
            Lwt.return 1
          | "y" ->
            Lwt_unix.sleep 3.0
            >>= fun _ ->
            let () = print_endline "returning y" in
            Lwt.return 2
          | _ -> M.error "no variable")
        (Plus (Var "x", Var "y"))
    in
    Format.printf "%d\n%!" @@ Lwt_main.run rez
  ;;
end
