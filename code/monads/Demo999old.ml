module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
end

module type EXTRA = sig
  include MONAD

  val sleep : int -> unit t
  val print_line : string -> unit t
  val both : 'a t -> 'b t -> ('a * 'b) t
end

module Work (M : EXTRA) = struct
  open M

  let mysleep n = sleep n >>= fun () -> print_line (Printf.sprintf "I slept %d seconds" n)
  let main = M.both (mysleep 3) (mysleep 3)
end

(* ********************** Identity ***************************************** *)
module Demo1 = struct
  module M : EXTRA with type 'a t = 'a = struct
    type 'a t = 'a

    let ( >>= ) x f = f x
    let return x = x
    let both a b = a, b
    let sleep n = Thread.delay (float_of_int n)
    let print_line = Stdlib.print_endline
  end

  let _ =
    print_endline "Demo with Identity monad";
    let module E = Work (M) in
    let _, _ = E.main in
    ()
  ;;
end

(* ********************** Threads ***************************************** *)
module Demo2 = struct
  module M : EXTRA with type 'a t = 'a Lwt.t = struct
    include Lwt

    let sleep n = Lwt_unix.sleep (float_of_int n)
    let print_line s = Lwt_io.(write_line stdout) s
    let both = Lwt.both
  end

  let _ =
    print_endline "Demo with Lwt monad";
    let module E = Work (M) in
    Lwt_main.run E.main
  ;;
end
