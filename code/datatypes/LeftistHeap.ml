(* https://github.com/ikatanic/okasaki-ocaml/blob/master/heap.ml *)

exception Empty

module type ORDERED = sig
  type t

  val compare : t -> t -> int
end

module type S = sig
  module Elem : ORDERED

  type t

  val empty : t

  val isEmpty : t -> bool

  val add : Elem.t -> t -> t

  val merge : t -> t -> t

  val findMin : t -> Elem.t
  (** raises Empty if heap is empty *)

  val deleteMin : t -> t
  (** raises Empty if heap is empty *)
end

module LeftistHeap (Element : ORDERED) = struct
  module Elem = Element

  type t = E | Tree of int * Elem.t * t * t

  let empty = E

  let isEmpty = function E -> true | _ -> false

  let rank = function E -> 0 | Tree (r, _, _, _) -> r

  let makeT x a b =
    if rank a >= rank b then Tree (rank b + 1, x, a, b)
    else Tree (rank a + 1, x, b, a)

  let rec merge h1 h2 =
    match (h1, h2) with
    | E, h -> h
    | h, E -> h
    | Tree (_, x, a, b), Tree (_, y, c, d) ->
        if Elem.compare x y < 0 then makeT x a (merge b h2)
        else makeT y c (merge h1 d)

  let add x = merge (Tree (1, x, E, E))

  let findMin = function
    | E -> raise Empty
    | Tree (_, x, _, _) -> x

  let deleteMin = function
    | E -> raise Empty
    | Tree (_, _, a, b) -> merge a b
end

module H = LeftistHeap (Int)

type label = Root | Labl of H.Elem.t

let pp_label ppf = function
  | Root -> Format.fprintf ppf "root"
  | Labl n -> Format.fprintf ppf "%d" n

let show_tree (root : H.t) =
  let filename = "/tmp/demo.dot" in
  let ch = open_out filename in
  let ppf = Format.formatter_of_out_channel ch in
  let rec helper parent tree =
    match tree with
    | H.E -> ()
    | Tree (_, s, l, r) ->
        Format.fprintf ppf "%d [label=\"%d\"];\n" s s;
        Format.fprintf ppf "%a -> %d;" pp_label parent s;
        helper (Labl s) l;
        helper (Labl s) r
  in
  let () =
    match root with
    | E -> ()
    | Tree (_, v, l, r) ->
        Format.fprintf ppf "digraph graphname {\n";
        Format.fprintf ppf "root [label=\"%d\"];\n" v;
        helper Root l;
        helper Root r;
        Format.fprintf ppf "}\n%!"
  in
  close_out ch;
  let _ =
    Sys.command
      (Format.asprintf "dot -Tpng %s -o /tmp/demo.png "
         filename)
  in
  let _ =
    Sys.command (Format.asprintf "xdg-open /tmp/demo.png ")
  in
  ()

let () =
  let set = H.empty in
  let set = H.(set |> add 3 |> add 4 |> add 5) in
  show_tree set;
  let set2 = H.(empty |> add 7) in
  show_tree set2;
  let set3 = H.merge set set2 in
  show_tree set3

let __ () =
  let set = H.empty in
  let set =
    H.(
      set |> add 5 |> add 12 |> add 7 |> add 13 |> add 19
      |> add 8)
  in
  show_tree set;
  let set2 = H.(H.empty |> add 6 |> add 10 |> add 9) in
  show_tree set2;
  let set3 = H.merge set set2 in
  show_tree set3

let __ () =
  let set = H.empty in
  let set = H.(set |> add 10) in
  show_tree set;
  let set = H.(set |> add 2) in
  show_tree set

let __ () =
  let set = H.(empty |> add 2 |> add 10 |> add 9) in
  show_tree set;
  let set2 = H.(empty |> add 3 |> add 6) in
  show_tree set2;
  let set3 = H.merge set set2 in
  show_tree set3
