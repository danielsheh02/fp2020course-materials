open Base

let run_repl _ = failwith "not implemented"

let run_single eval =
  let open Lambda_lib in
  let text = Stdio.In_channel.(input_all stdin) |> String.rstrip in
  (* Format.printf "text: '%s'\n%!" text; *)
  let ast = Parsing.parse (fun p -> p.expr_long p) text in
  match ast with
  | Error s -> Format.printf "Error: %s\n%!" s
  | Result.Ok ast ->
    (* Format.printf "Parsed: %a\n%!" Pprintast.pp ast; *)
    let rez = eval ast in
    Format.printf "Result: %a\n%!" Pprintast.pp rez
;;

type strategy =
  | CBN
  | CBV
  | NO

type opts =
  { mutable batch : bool
  ; mutable stra : strategy
  }

let () =
  let opts = { batch = false; stra = CBN } in
  Arg.parse
    [ "-", Arg.Unit (fun () -> opts.batch <- true), "read from stdin"
    ; "-no", Arg.Unit (fun () -> opts.stra <- NO), "normal order"
    ; "-cbv", Arg.Unit (fun () -> opts.stra <- CBV), "CBV"
    ; "-cbn", Arg.Unit (fun () -> opts.stra <- CBN), "CBN"
    ]
    (fun _ -> assert false)
    "asdf";
  let eval =
    Lambda_lib.Lambda.apply_strat
      (match opts.stra with
      | NO -> Lambda_lib.Lambda.nor_strat
      | CBV -> Lambda_lib.Lambda.cbv_strat
      | CBN -> Lambda_lib.Lambda.cbn_strat)
  in
  (if opts.batch then run_single else run_repl) eval
;;
