(* https://kcsrk.info/cs3100_f19/lectures/lec14/lec14.pdf *)

(** Normal lists *)
let rec ones = 1 :: ones

let rec zero_ones = 0 :: 1 :: zero_ones

let%test _ =
  try
    let _zero_ones_string =
      List.map string_of_int zero_ones
    in
    false
  with Stack_overflow -> true

(* Обычный список *)
type 'a list = 'a Stdlib.List.t =
  | []
  | ( :: ) of 'a * 'a list

module Bad = struct
  (* Вот такое не сработает *)
  type 'a stream = Cons of 'a * 'a stream
end

(* Приостановка исполнения *)
let f () = failwith "error"

let%test _ =
  try
    let _ = f () in
    false
  with Failure _ -> true

(* А вот это правильно *)
(* Так сделано в модуле Seq *)
type 'a seq = Cons of 'a * (unit -> 'a seq) | Nil

let rec zero_ones =
  Cons (0, fun () -> Cons (1, fun () -> zero_ones))

(* Don't use it in production code base, if you are not sure *)
let hd_exn = function
  | Nil -> failwith "bad argument"
  | Cons (x, _) -> x

(* Don't use it in production code base, if you are not sure *)
let tl_exn = function
  | Nil -> failwith "bad argument"
  | Cons (_, xs) -> xs ()

let rec take n s =
  if n = 0 then [] else hd_exn s :: take (n - 1) (tl_exn s)

let%test _ =
  take 10 zero_ones = [ 0; 1; 0; 1; 0; 1; 0; 1; 0; 1 ]

let rec drop n s =
  if n = 0 then s else drop (n - 1) (tl_exn s)

let rec map f s =
  Cons (f (hd_exn s), fun () -> map f (tl_exn s))

let zero_ones_str = map string_of_int zero_ones

let%test _ =
  take 10 zero_ones_str
  = [ "0"; "1"; "0"; "1"; "0"; "1"; "0"; "1"; "0"; "1" ]

let rec filter p s =
  if p (hd_exn s) then filter p (tl_exn s)
  else Cons (hd_exn s, fun () -> filter p (tl_exn s))

let%test _ =
  let s' = filter (( = ) 0) zero_ones in
  take 10 s' = [ 1; 1; 1; 1; 1; 1; 1; 1; 1; 1 ]

let rec zip f s1 s2 =
  Cons
    ( f (hd_exn s1) (hd_exn s2),
      fun () -> zip f (tl_exn s1) (tl_exn s2) )

(**
  * EXERCISE:  Fibonacci
  *)
let rec fib =
  Cons
    ( 1,
      fun () ->
        Cons (1, fun () -> zip ( + ) fib (tl_exn fib)) )

let%test _ =
  take 10 fib = [ 1; 1; 2; 3; 5; 8; 13; 21; 34; 55 ]

(**
  *  Erathosphene's prime numbers?
  *  EXERCISE
  *)

(* *************** Ещё более ленивые списки ************** *)

(* Проблема: Оно считает повторно одно и то же *)

let () =
  let c = ref 0 in
  let xs : unit seq = map (fun _ -> incr c) zero_ones in

  let (_ : unit list) = take 10 xs in
  assert (!c == 11);
  let (_ : unit list) = take 10 xs in
  assert (!c == 21)

type 'a stream = Nil | Cons of 'a * 'a stream Lazy.t

let rec zero_ones = Cons (0, lazy (Cons (1, lazy zero_ones)))

let rec map f = function
  | Nil -> Nil
  | Cons (x, (lazy s)) -> Cons (f x, lazy (map f s))

let hd_exn = function
  | Nil -> failwith "bad argument"
  | Cons (x, _) -> x

let tl_exn = function
  | Nil -> failwith "bad argument"
  | Cons (_, (lazy tl)) -> tl

let rec take n s =
  if n = 0 then [] else hd_exn s :: take (n - 1) (tl_exn s)

let () =
  let c = ref 0 in
  let xs : unit stream = map (fun _ -> incr c) zero_ones in

  let (_ : unit list) = take 10 xs in
  assert (!c == 11);
  let (_ : unit list) = take 10 xs in
  assert (!c == 11)
